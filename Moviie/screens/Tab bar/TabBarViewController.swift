//
//  TabBarViewController.swift
//  Moviie
//
//  Created by Chính Trình Quang on 12/1/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.tintColor = #colorLiteral(red: 0.3568627451, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
        tabBar.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        let homeVC = HomeViewController()
        homeVC.tabBarItem = UITabBarItem(title: "Home", image: #imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), tag : 0)
        
        
        let searchVC = SearchViewController()
        searchVC.tabBarItem = UITabBarItem(title: "Search", image: #imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), tag : 1)
        let nav2 = UINavigationController(rootViewController: searchVC)
        
        let comingSoonVC = ComingSoonViewController()
        comingSoonVC.tabBarItem = UITabBarItem(title: "Coming Soon", image: #imageLiteral(resourceName: "desktop").withRenderingMode(.alwaysOriginal), tag : 2)
        let nav3 = UINavigationController(rootViewController: comingSoonVC)
        
        let downloadVC = DownloadViewController()
        downloadVC.tabBarItem = UITabBarItem(title: "Search", image: #imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), tag : 3)
        let nav4 = UINavigationController(rootViewController: downloadVC)
        
        let settingVC = SettingViewController()
        settingVC.tabBarItem = UITabBarItem(title: "Setting", image: #imageLiteral(resourceName: "equalizer").withRenderingMode(.alwaysOriginal), tag : 4)
        let nav5 = UINavigationController(rootViewController: settingVC)
        let tabBarList = [homeVC, nav2, nav3, nav4, nav5]
        viewControllers = tabBarList
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
